package se.kth.myseries.client.utils.notifications;

/**
 * MySeries <br>
 * Observer that is notified of changes on the watchlist
 * 
 * @author Marcelo
 * @since 25/11/2010
 */
public interface Observer {
	
	public void update();

}
