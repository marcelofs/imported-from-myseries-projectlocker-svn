package se.kth.myseries.shared.exceptions;

/**
 * MySeries <br>
 * 
 * @author Marcelo
 * @since 13/12/2010
 */
public class SocialNetworkException extends Exception {
	
	private static final long	serialVersionUID	= 1L;
	
	protected SocialNetworkException(final String message) {
		super(message);
	}

}
