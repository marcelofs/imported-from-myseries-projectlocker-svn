package se.kth.myseries.server.watchlist;

import se.kth.myseries.client.watchlist.IWatchlistService;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * MySeries <br>
 * @author Marcelo
 * @since  08/10/2010
 */
public class WatchlistServiceImpl extends RemoteServiceServlet implements
		IWatchlistService {

}
