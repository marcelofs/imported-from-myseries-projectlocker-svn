package se.kth.myseries.client.watchlist;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * MySeries <br>
 * @author Marcelo
 * @since  08/10/2010
 */
@RemoteServiceRelativePath("calendar")
public interface IWatchlistService extends RemoteService {
	
}
