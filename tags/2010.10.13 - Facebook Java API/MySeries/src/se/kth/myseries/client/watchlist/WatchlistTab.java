package se.kth.myseries.client.watchlist;

import com.smartgwt.client.widgets.tab.Tab;

/**
 * MySeries <br>
 * A tab with the full calendar for the user. <br>
 * Shows all episodes from the user's list that aired/will air in XX weeks. <br>
 * 
 * @author Marcelo
 * @since 07/10/2010
 */
public class WatchlistTab extends Tab {
	
	public WatchlistTab() {
		super("Watchlist", "calendar.png");
		build();
	}
	
	/**
	 * Builds the user interface
	 */
	private void build() {
		
		// TODO
	}
	
}
