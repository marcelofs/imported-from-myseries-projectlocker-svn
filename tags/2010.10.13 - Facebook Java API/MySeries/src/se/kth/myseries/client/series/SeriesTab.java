package se.kth.myseries.client.series;

import com.smartgwt.client.widgets.tab.Tab;

/**
 * MySeries <br>
 * A tab with a list of series available for the user to "follow". <br>
 * Has a filtering function to filter for free-text (serie's name) or category
 * 
 * @author Marcelo
 * @since 07/10/2010
 */
public class SeriesTab extends Tab {
	
	public SeriesTab() {
		super("Series", "television.png");
		build();
	}
	
	/**
	 * Builds the user interface
	 */
	private void build() {
		
		// TODO
	}
	
}
