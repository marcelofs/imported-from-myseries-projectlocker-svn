package se.kth.myseries.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * MySeries <br>
 * Data Transfer Object for the Season entity
 * 
 * @author Marcelo
 * @since 08/10/2010
 */
public class SeasonDTO implements IsSerializable {
	
}
